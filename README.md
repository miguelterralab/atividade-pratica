# TerraLAB

## Atividade Prática - Sprint 1

##### Miguel de Azevedo

## Questões Teóricas

> **1. O que é Git?** \
> **R:** Git é um sistema de controle de versão amplamente utilizado para gerenciar e manter bases de código, permitindo o acompanhamento e registro das mudanças ao longo do desenvolvimento de um projeto.

> **2. O que é a staging area?** \
> **R:** A área de staging (ou área de preparação) representa o conjunto de arquivos que estão prontos para serem incluídos no próximo commit, ou seja, são os arquivos selecionados para fazer parte do próximo snapshot do código.

> **3. O que é o working directory?** \
> **R:** O diretório de trabalho é o conjunto de arquivos do repositório presentes no sistema de arquivos local, onde são feitas as modificações e atualizações do código.

> **4. O que é um commit?** \
>**R:** Um commit no Git é a captura do estado atual da staging area no momento da criação. Ele armazena as alterações feitas, permitindo registrar o conteúdo dos arquivos e as diferenças entre versões.

> **5. O que é uma branch?** \
> **R:** Uma branch é uma ramificação do código, criando uma bifurcação no histórico do versionamento, possibilitando que diferentes conjuntos de alterações sejam desenvolvidos sem interferir uns nos outros. É também o caminho para unir essas ramificações através de merges, embora conflitos possam surgir.

> **6. O que é o head no Git?** \
> **R:** O HEAD no Git é um ponteiro para um commit específico, indicando o estado atual da ramificação ou da linha do tempo do repositório.

> **7. O que é um merge?** \
> **R:** Um merge é a operação de combinar duas branches distintas, incorporando as modificações de ambas as ramificações em uma única branch.

> **8. Explique os 4 estados de um arquivo no Git.** \
> **R:** Os 4 estados de um arquivo no Git incluem:
> - Untracked: Arquivos novos ou ausentes no último commit.
> - Unmodified: Arquivos no último commit que não foram alterados.
> - Modified: Arquivos alterados desde o último commit, mas não incluídos na área de preparação (staging area).
> - Staged: Arquivos alterados ou novos, prontos para o próximo commit após serem adicionados à área de preparação.

> **9. Explique o comando git init.** \
> **R:** O comando git init cria um repositório Git vazio na pasta atual, se ainda não existir, criando uma pasta oculta ".git" e inicializando as configurações do repositório.

> **10. Explique o comando git add.** \
> **R:** O comando git add seleciona arquivos untracked ou modificados e os adiciona à área de preparação (staging area).

> **11. Explique o comando git status.** \
> **R:** O comando git status exibe o estado atual do diretório de trabalho, mostrando quais arquivos estão em quais estados (untracked, unmodified, modified, staged) e quais mudanças estão prontas para serem incluídas no próximo commit.

> **12. Explique o comando git commit.** \
> **R:** O comando git commit registra as alterações feitas no repositório, salvando o estado atual das modificações presentes na área de preparação (staging area).

> **13. Explique o comando git log.** \
> **R:** O comando git log exibe o histórico de commits do repositório, permitindo visualizar informações como a mensagem do commit, data de criação, autor e as diferenças introduzidas por cada commit.

> **14. Explique o comando git checkout -b.** \
> **R:** O comando git checkout -b muda o estado atual do diretório de trabalho para uma branch existente. Se a opção "-b" for utilizada com um nome de branch inexistente, uma nova branch será criada.

> **15. Explique o comando git reset e suas três opções.** \
> **R:** O comando git reset possui três opções:
> - Soft: Move o HEAD para o commit indicado, mantendo as mudanças no diretório de trabalho e na área de preparação.
> - Hard: Move o HEAD para o commit indicado, desfazendo todas as alterações no diretório de trabalho e na área de preparação.
> - Mixed: Move o HEAD para o commit indicado, mantendo as alterações no diretório de trabalho, mas desfazendo as alterações na área de preparação.

> **16. Explique o comando git revert.** \
> **R:** O comando git revert desfaz as alterações de um commit específico criando um novo commit que anula as mudanças introduzidas por esse commit.

> **17. Explique o comando git clone.** \
> **R:** O comando git clone copia um repositório remoto para um novo diretório na máquina local, criando uma cópia idêntica do repositório remoto.

> **18. Explique o comando git push.** \
> **R:** O comando git push atualiza o repositório remoto com os novos commits e os arquivos associados.
# Atividade Prática - README.md

Este README.md é um registro das ações realizadas e respostas referentes à atividade prática.

## Perguntas e Respostas

> **1. A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.**

> **2. Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o [artigo](https://raullesteves.medium.com/github-como-fazer-um-readme-md-bonit%C3%A3o-c85c8f154f8) como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão:**
> - README.md onde o trainne irá continuamente responder as perguntas em formas de commit.
> - Inserção de código, exemplo de commit de feature:
>   - *git commit -m "{PERGUNTA}" -m "{RESPOSTA}"*

>**3. Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:**
> ```js
>const args = process.argv.slice(2);
>console.log(parseInt(args[0]) + parseInt(args[1]));
> ```
> **Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:** \
> **R:** Para executá-lo, uso o comando `node calculadora.js a b`.

>**4. Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.** \
> **R:**  Adicionei o arquivo ao commit utilizando o comando `git add calculadora.js`.
>
>**Que tipo de commit esse código deve ter de acordo ao conventional commit.** \
> **R:** O tipo de commit usado foi `feat: MENSAGEM`.
>
>**Que tipo de commit o seu README.md deve contar de acordo ao conventional commit.** \
> **R:** 
O README.md foi adicionado no commit com tipo `docs: MENSAGEM` e por fim, faça um push desse commit.**

> **5. Copie e cole o código abaixo em sua calculadora.js:**
> ```js
>const soma = () => {
>   console.log(parseInt(args[0]) + parseInt(args[1]));
>};
> 
>const args = process.argv.slice(2);
> 
>soma();
> ```
> **Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.** \
> **R:** Fiz o commit utilizando o tipo `refactor: MENSAGEM`.

>**6. João entrou em seu repositório e o deixou da seguinte maneira:**
> ```js
>const soma = () => {
>    console.log(parseInt(args[0]) + parseInt(args[1]));
>};
>
>const sub = () => {
>    console.log(parseInt(args[0]) - parseInt(args[1]));  
>}
>
>const args = process.argv.slice(2);
>
>switch (args[0]) {
>    case 'soma':
>        soma();
>    break;
>
>    case 'sub':
>        sub();
>    break;
>    
>    default:
>        console.log('does not support', args[0]);
>}
> ```
> **Depois disso, realizou um git add . e um commit com a mensagem: "Feature: added subtraction"**
>
> **Faça como ele e descubra como executar o seu novo código.** \
> **R:** O comando para executar é `node calculadora.js operacao a b`.

> **Nesse código, temos um pequeno erro, encontre-o e corrija para que a soma e <s>divisão</s> subtração funcionem.** \
> **R:** Em ambas as funções, `soma()` e `sub()`, os números estavam com os índices errados do array args. Eles estavam como `parseInt(args[0])+parseInt(args[1])`, porém deveriam estar como `parseInt(args[1]) + parseInt(args[2])` pois o `args[0]` já é a operação.
No bloco default do switch, o array estava escrito como `arg[0]` enquanto deveria ser `args[0]`.

>  **7. Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.**
> **Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.** \
> **R:** O comando utilizado foi `git checkout -b NOME_BRANCH`.

> **8. Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request.** \
**Em seu GitLab, descubra como realizá-lo de acordo com o GitFlow.** \
**R:** Criei uma branch de staging a partir da branch de desenvolvimento, testei o código e realizei o merge com a branch principal. E atualizei a develop com a alteração nova na main.

>**9. João quis se redimir dos pecados e fez uma melhoria em seu código, mas ainda assim, continuou fazendo um push na master, fez a seguinte alteração no código e fez o commit com a mensagem:** \
> `feat: add conditional evaluation`
> ```js
>var x = args[0];
>var y = args[2];
>var operator = args[1];
>
>function evaluate(param1, param2, operator) {
>  return eval(param1 + operator + param2);
>}
>
>if ( console.log( evaluate(x, y, operator) ) ) {}
>```
> **Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, e seu chefe não descobriu que tudo está comprometido, faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!**

> **10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem.** \
> **R:** O código usa a função `eval()` para executar operações matemáticas. Execute-o com o comando `node calculadora.js a operacao b` (sendo "a" e "b" números e "operacao" um símbolo de operador).
